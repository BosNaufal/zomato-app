import * as React from 'react';
import { cx, css } from 'emotion'
import styled from 'react-emotion';


export const Form = styled('form')`
  padding: 0 15px;
`

export const Group = styled('div')`
  margin-bottom: 5px;
`

export const Label = styled('label')`
  display: block; 
  margin-bottom: 3px;
  color: #444;
`

export const BaseInput = () => css`
  display: block; 
  width: 100%;
  border: 2px solid #cdcdcd;
  border-radius: 5px;
  padding: 7px 10px;
  outline: 0;
  margin-bottom: 20px;

  &:focus {
    border-color: var(--primary);
  }
`

export const Input = styled('input')`
  ${BaseInput}
`

export const Textarea  = styled('textarea')`
  ${BaseInput}
  min-height: 150px;
`


const SelectWrapper = styled("div")`
  border: 2px solid #cdcdcd;
  position: relative;
  border-radius: 5px;
  margin-bottom: 20px;

  &::after {
    top: 40%;
    right: 7px;
    content: "";
    position: absolute;
    border: 5px solid transparent;
    border-top-color: black;
  }
`

const SelectType = styled('select')`
  position: relative;
  width: 100%;
  outline: 0;
  background: none;
  display: block;
  appearance: none; 
  padding: 8px 10px;
  padding-right: 25px;
  border-radius: 0;
  border: 0;

  &:focus {
    border-color: var(--primary);
  }
`
export const Select = ({ className, style, ...rest }: React.SelectHTMLAttributes<HTMLSelectElement>) => (
  <SelectWrapper className={className} style={style}>
    <SelectType {...rest}>
      {rest.children}
    </SelectType>
  </SelectWrapper>
) 


export interface ICheckboxProps {
  className?: string
  style?: object
  type?: string
  color?: string
  label?: string
  round?: boolean,
  HTMLProps?: React.InputHTMLAttributes<HTMLInputElement>
}

const CheckboxWrapper = styled("div")`
  margin-right: 7px;

  & > .pretty .state label:before {
    border-color: #DDD;
  }
`

export const Checkbox = ({ 
  color = "success",
  round = false,
  className = "", 
  style = {}, 
  label = "",
  HTMLProps 
}: ICheckboxProps) => (
  <CheckboxWrapper style={style} className={cx("pretty p-default", className, { 'p-round': round })}>
    <input type="checkbox" {...HTMLProps} />
    <div className={cx("state", `p-${color}-o`)}>
      <label>{label}</label>
    </div>
  </CheckboxWrapper>
)