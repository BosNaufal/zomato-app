import styled from 'react-emotion';
import { Link } from 'react-router-dom'

export const Button = styled("button")`
  outline: none;
  margin-right: 10px;
  padding: 10px 15px;
  border-radius: 7px;
  font-size: 1.1em;

  background: var(--primary);
  color: white;

  ${({ danger = false }: any) => danger && ({
    background: 'var(--red)',
  })}

  ${({ secondary = false }: any) => secondary && ({
    background: 'var(--secondary)',
    color: 'black',
  })}

  border: 0;
  cursor: pointer;

  /* box-shadow: 1px 2px 5px 0 rgba(0,0,0,.3); */
  box-shadow: 1px 2px 5px 0 rgba(0,0,0,.2);

  display: block;
  ${({ block = false }: any) => block && ({
    display: 'flex',
  })}

  align-items: center;
  text-align: center;

  i {
    font-size: 1.2em;
    margin-right: 10px;
  }
`

export const FloatButton = styled(Link)`
  display: block;
  position: fixed;
  background: var(--secondary);
  color: #2b2b2b;
  bottom: 35px;
  right: 25px;
  z-index: 99;
  font-size: 2em;
  width: 50px;
  height: 50px;
  border-radius: 50%;
  display: flex;
  align-items: center;
  justify-content: center;
  cursor: pointer;
  box-shadow: 0px 1px 6px 0 rgba(0,0,0,.3), 0 4px 7px 1px rgba(0,0,0,.2);
`

export const BasicCard = styled('div')`
  background: white;
  box-shadow: 0px 1px 6px 0 rgba(0,0,0,.1), 0 4px 4px 0 rgba(0,0,0,.1);
  border-radius: 5px;
  padding: 5px 15px 10px;
  margin-bottom: 25px;
`