import * as React from 'react';
import styled from 'react-emotion'
import { NavLink } from 'react-router-dom'

// export interface NavbarProps {
  
// }
 
class MainNavbar extends React.Component<{}, {}> {
  render() { 
    return ( 
      <Wrapper>
        <Inner className="mini-container">
          <Title>Zomato App</Title>
          <Menubar>
            <Menu to={'/'}>Home</Menu>
            {/* <Menu to={'/budgets'}>Budgeting</Menu>
            <Menu to={'/ticketing'}>Ticketing</Menu>
            <Menu to={'/scanner/1'}>Scanner</Menu> */}
          </Menubar>
        </Inner>
      </Wrapper>
    );
  }
}

const Wrapper = styled('div')`
  position: relative;
  background: var(--primary);
  color: white;
  /* box-shadow: 0px 1px 6px 0 rgba(0,0,0,.3), 0px 2px 4px 0 rgba(0,0,0,.1); */
  box-shadow: 0px 1px 2px 0 rgba(0,0,0,.1), 0px 2px 4px 0 rgba(0,0,0,.1);
`

const Inner = styled('div')`
  padding-left: 0;
  padding-right: 0;
`

const Title = styled('h1')`
  margin: 0;
  font-size: 1.3em;
  padding: 10px 15px 3px;
`

const Menubar = styled('div')`
  padding-left: 7px; 
  display: flex;
  overflow-x: auto;

  &::-webkit-scrollbar {
    display: none;
  }
`

const Menu = styled(NavLink)`
  display: block;
  padding: 10px 10px 7px;
  cursor: pointer;
  color: white;
  text-decoration: none;
  border-bottom: 3px solid transparent;

  &.active {
    font-weight: 700;
    border-color: var(--secondary);
  }
`

export default MainNavbar;
