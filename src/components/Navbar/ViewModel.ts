import { observable, action } from 'mobx';

class ViewModel {
  @observable show: boolean = true
  @observable backButton: boolean = true
  @observable type: string = "main"

  @observable text: string = "Menu Text"

  @action setBackButton = (withBackButton: boolean): void => {
    this.backButton = withBackButton
  }

  @action setType = (type: string): void => {
    this.type = type
  }

  @action setShow = (isShow: boolean): void => {
    this.show = isShow
  }

  @action setText = (text: string): void => {
    this.text = text
  }
}

const store = new ViewModel()
export default store
