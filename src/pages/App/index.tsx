import * as React from 'react';
import { hot } from 'react-hot-loader'
import { injectGlobal } from 'emotion'
import { configure } from 'mobx'
import { ToastContainer, toast } from 'react-toastify';

import 'react-toastify/dist/ReactToastify.css';
import 'pretty-checkbox/dist/pretty-checkbox.css'

import Navbar from 'src/components/Navbar/index';
import Routes from './Routes'

configure({
  enforceActions: 'observed',
})

class App extends React.Component {
  public render() {
    return (
      <div>
        <div className="MainLayout">
          <Navbar />
          <div className="MainContent">
            <div className="MainContentScroller">
              <Routes />
            </div>
          </div>
        </div>
        <ToastContainer 
          position={toast.POSITION.TOP_CENTER}
          autoClose={3000}
        />
      </div>
    );
  }
}

// tslint:disable-next-line
injectGlobal`
  * {
    box-sizing: border-box;
  }

  body {
    /* --body-background: #f3fff6; */
    /* --body-background: #f8fffa; */
    /* --body-background: #f6f9f7; */
    /* --primary: #00cc75; */
    /* --primary: #3bb16f; */
    /* --primary: #43A047;
    --primary-light: #E8F5E9; */
    /* --primary: #25cab0; */
    --primary: #ca2525;

    /* --primary: #299c6b; */
    --secondary: #ffee58;
    --darker: #299c6b;
    --white: #fff;
    --black: #2b2b2b;
    --orange: #f1c40f;
    --red: #E53935;
    --red-surface: #FFCDD2;

    /* font-family: "Lato", sans-serif; */
    font-family: "Roboto", sans-serif;

    font-size: 1em;
    line-height: 1.4;
    /* background: #f9f9f9; */
    /* background: #fffaf7; */

    position: relative;
    width: 100%;
    height: 100vh;
    /* background: var(--body-background); */
  }

  #root {
    overflow: auto;
  }

  img {
    width: 100%;
  }

  .fluid, #root {
    position: relative;
    width: 100%;
    height: 100%;
  }

  .MainLayout {
    display: flex;
    flex-direction: column;
    height: 100vh;
  }

  .MainContent {
    flex: 1;
    display: flex;
    overflow: auto;
    padding-top: 20px;
    padding-bottom: 35px;
  }

  .MainContentScroller {
    min-height: min-content;
    width: 100%;
  }

  .MainWrapper {
    overflow-x: hidden;
    padding-top: 85px;
    /* padding-bottom: 100px; */
  }

  .light-drop-shadow {
    filter: drop-shadow(2px 2px 2px var(--black));
  }

  .mini-container {
    max-width: 600px;
    padding: 0 10px;
    display: block;
    margin: 0 auto;
  }

  .text-center {
    text-align: center;
  }

`

export default hot(module)(App)
