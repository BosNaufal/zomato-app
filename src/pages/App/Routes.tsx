import * as React from 'react';
import { Route } from 'react-router-dom'

import RestoListPage from 'src/pages/Resto/List/index'
import RestoDetailPage from 'src/pages/Resto/Detail/index'


class Routes extends React.Component<{}, {}> {
  public render() { 
    return (
      <div>
        <Route exact={true} path="/" component={RestoListPage} />
        <Route exact={true} path="/restaurant/:restoId" component={RestoDetailPage} />
      </div>
    );
  }
}
 
export default Routes;