import { observable, action } from 'mobx';

class GlobalStore {
  @observable user: object = {}

  @action setModelProps = <K extends keyof GlobalStore>(key: K, payload: Partial<GlobalStore>): void => {
    this[key] = payload
  }
}

const store = new GlobalStore()
export default store
