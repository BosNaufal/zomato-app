import * as React from 'react';
import styled from 'react-emotion'
import { BasicCard } from 'src/components/Styled/General';
import { observer } from 'mobx-react';
 
import store from './store'

class RestoHeader extends React.Component<{}, {}> {
  render() { 
    const { resto } = store
    return resto && resto.id ? (
      <BasicCard>
        {resto.thumb?
          <ImageWrapper>
            <img src={resto.thumb} alt={resto.name} />
          </ImageWrapper>
        :null}
        <Header>
          <Title>{resto.name}</Title>
        </Header>
        <Meta>
          <Rating color={resto.user_rating.rating_color}>{resto.user_rating.rating_text}</Rating>
          {/* <Votes>{resto.user_rating.votes} Votes</Votes> */}
          <Location>{resto.location.address}, {resto.location.city}</Location>
        </Meta>
        <RestoType>{resto.cuisines}</RestoType>
      </BasicCard>
    ) : null;
  }
}

const Header = styled('div')`
  display: flex;
  margin-top: 8px;
  margin-bottom: 3px;
  align-items: center;
`

const ImageWrapper = styled('div')`
  border-radius: 5px;
  overflow: hidden;
  margin: 0 -15px;
  margin-top: -5px;

  img {
    width: 100%;
  }
`

const Title = styled('div')`
  display: block;
  font-size: 1.2em;
  font-weight: 700;
  flex: 1;
  color: black;
  text-decoration: none;
`

const Meta = styled('div')`
  margin-bottom: 7px;
  
`

const Rating = styled('div')`
  display: inline-block;
  background: #${({ color }: any) => color};
  color: white;
  font-size: 1em;
  font-weight: 700;
  padding: 3px 15px;
  border-radius: 5px;
  margin-right: 10px;
`

// const Votes = styled('div')`
//   display: inline-block;
//   font-size: 0.8em;
// `

const RestoType = styled('div')`
  font-size: 0.85em;
  color: #444;
`

const Location = styled('div')`
  font-size: 1em;
  margin-top: 7px;
  margin-bottom: -5px;
`

 
export default observer(RestoHeader);