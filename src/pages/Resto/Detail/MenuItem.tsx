import * as React from 'react';
import styled from 'react-emotion';

interface MenuItemProps {
  dish: any
}

class MenuItem extends React.Component<MenuItemProps, {}> {
  render() { 
    const { dish } = this.props
    return ( 
      <Wrapper>
        <Header>
          <Title>{dish.name}</Title>
          <RightSide>
            <Label>{dish.price}</Label>
          </RightSide>
        </Header>
      </Wrapper>
    );
  }
}


const Wrapper = styled('div')`
  background: white;
  border-bottom: 1px solid #DDD;
  padding: 5px 0 13px;
  &:last-child {
    border: 0;
  }
`

const Header = styled('div')`
  display: flex;
  margin-top: 8px;
  align-items: center;
`

const Title = styled('div')`
  display: block;
  font-size: 0.9em;
  flex: 1;
  color: #444;
  text-decoration: none;
`

const RightSide = styled('div')`
  align-self: flex-start;
  margin-left: 7px;
`

const Label = styled('div')`
  font-size: 0.8em;
  cursor: pointer;
  padding: 3px 10px;
  border-radius: 35px;
  background: var(--secondary);
`

export default MenuItem;