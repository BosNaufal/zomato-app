import * as React from 'react';
import { hot } from 'react-hot-loader'
import { RouteComponentProps } from 'react-router-dom'
import { observer } from 'mobx-react';


import Header from './Header';
import MenuList from './MenuList';

import NavbarViewModel from 'src/components/Navbar/ViewModel'
import store from './store';

class EventDetailPage extends React.Component<RouteComponentProps<{restoId: string}>, {}> {
  componentDidMount() {
    store.netralize()
    NavbarViewModel.setType("flat")
    NavbarViewModel.setText("Restaurant Detail")
    store.getDetail(this.props.match.params.restoId)
  }

  componentWillUnmount() {
    NavbarViewModel.setType("main")
  }

  render() { 
    return (
      <div>
        <div className="mini-container">
          <Header />
          {store.resto && store.resto.id &&
            <MenuList />
          }
        </div>
      </div>
    );
  }
}
 
export default hot(module)(observer(EventDetailPage));