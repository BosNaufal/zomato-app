import { observable, action } from 'mobx';
import { toast } from 'react-toastify';
import Req from 'src/api/base'


class RestoDetailStore {
  @observable resto: any = {}
  @observable menuList: [] = []
  @observable loadingHeader: boolean = false
  @observable loadingMenuList: boolean = false

  @action setLoadingHeader = (payload: boolean) => {
    this.loadingHeader = payload
  }

  @action setLoadingMenuList = (payload: boolean) => {
    this.loadingMenuList = payload
  }

  @action netralize = () => {
    this.resto = {}
    this.menuList = []
  }

  @action setResto = (payload: object): void => {
    this.resto = payload
  }

  @action setMenuList = (payload: []): void => {
    this.menuList = payload
  }

  @action getDetail = async (restoId: string): Promise<object | boolean> => {
    this.setLoadingHeader(true)
    const res = await Req('/restaurant', {
      params: {
        res_id: restoId
      }
    })
    if (res.error) {
      this.setResto({})
      toast.error("Failed to load data...")
      return false
    } 
    this.setResto(res.data)
    this.setLoadingHeader(false)
    return res
  }

  @action getMenuList = async (): Promise<object | boolean> => {
    this.setLoadingMenuList(true)
    const res = await Req('/dailymenu', {
      params: {
        res_id: this.resto.id,
      }
    })
    if (res.error) {
      this.setMenuList([])
      return false
    } 
    const { daily_menus } = res.data
    const flatList = daily_menus.map((menu: any) => menu.daily_menu)
    this.setMenuList(flatList)
    this.setLoadingMenuList(false)
    return res
  }
}

const store = new RestoDetailStore()
export default store
