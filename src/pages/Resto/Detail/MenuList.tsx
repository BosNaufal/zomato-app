import * as React from 'react';
import { observer } from 'mobx-react'
import styled from 'react-emotion';
import * as moment from 'moment'

import { BasicCard } from 'src/components/Styled/General';

import MenuItem from './MenuItem';
import store from './store';
 
class MenuList extends React.Component<{}, {}> {
  componentDidMount() {
    store.getMenuList()
  }
  render() { 
    return ( 
      <List>
        <Title>Daily Menus</Title>
        {store.menuList.length ? store.menuList.map((menu: any, index: number) => (
          <BasicCard key={index}>
            <CategoryTitle>{menu.name}</CategoryTitle>
            <DateText>{moment(menu.start_date).format('d, MMMM YYYY')}</DateText>
            {menu.dishes.map((item: any, key: number) => (
              <MenuItem key={key} dish={item.dish} />
            ))}
          </BasicCard>
        )) 
        : 
          <BasicCard>
            <CategoryTitle>Sorry, we have no daily menu right now...</CategoryTitle>
          </BasicCard>
        }
      </List>
    );
  }
}


const List = styled('div')`
`
const Title = styled('h2')`
  padding-left: 7px;
  margin-top: 35px;
  margin-bottom: 10px;
  font-weight: 400;
  font-size: 1.3em;
  color: #444;
`

const CategoryTitle = styled('div')`
  padding-top: 5px;
  font-weight: 700;
`

const DateText = styled('div')`
  font-size: 0.9em;
`
 
export default observer(MenuList);