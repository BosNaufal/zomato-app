import { observable, action } from 'mobx';
import { toast } from 'react-toastify';
import Req from 'src/api/base'

type PageParams = {
  start: number, 
  count: number,
}

class HomeStore {
  @observable restoList: [] = []
  @observable loading: boolean = false
  @observable params: PageParams = {
    start: 0,
    count: 10
  }

  @action setParams = (payload: Partial<PageParams>): void => {
    this.params = { ...this.params, ...payload }
  }

  setNextPageParams = () => {
    const { start, count } = this.params
    const nextStart = start + count
    this.setParams({ start: nextStart })    
  }

  @action setLoading = (payload: boolean) => {
    this.loading = payload
  }

  @action setRestoList = (payload: []) => {
    this.restoList = payload
  }

  @action addMoreRestoList = (payload: []) => {
    const newRestoList = [...this.restoList]
    this.restoList = newRestoList.concat(payload) as []
  }

  @action getRestaurantList = async (isLoadmore: boolean = false): Promise<object | boolean> => {
    this.setLoading(true)
    const res = await Req('/search', {
      params: this.params,
    })
    if (res.error) {
      toast.error("Failed to load data...")
      this.setLoading(false)
      return false
    } 
    const { restaurants } = res.data
    const flatList = restaurants.map((item: any) => item.restaurant)
    if (!isLoadmore) {
      this.setRestoList(flatList)
    } else {
      this.addMoreRestoList(flatList)
    }
    this.setLoading(false)
    return res
  }
}

const store = new HomeStore()
export default store
