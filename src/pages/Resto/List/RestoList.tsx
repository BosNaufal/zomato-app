import * as React from 'react';
import { observer } from 'mobx-react';
import styled from 'react-emotion'
import RestoItem from './RestoItem';
import store from './store';
import { Button } from 'src/components/Styled/General'

class RestoList extends React.Component<{}, {}> {
  componentDidMount() {
    store.getRestaurantList()
  }

  handleLoadMore = () => {
    store.setNextPageParams()
    store.getRestaurantList(true)
  }

  render() { 
    return (
      <List>
        {store.restoList.map((resto, index) => (
          <RestoItem key={index} resto={resto} />
        ))}
        <ButtonWrapper>
          <Button secondary={true} disabled={store.loading} onClick={this.handleLoadMore}>{store.loading ? "Loading..." : "Load More"}</Button>
        </ButtonWrapper>
      </List>
    );
  }
}

const List = styled('div')`
  margin-bottom: 25px;
`

const ButtonWrapper = styled('div')`
  display: flex;
  justify-content: center;
`

 
export default observer(RestoList);