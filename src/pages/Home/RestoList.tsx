import * as React from 'react';
import { observer } from 'mobx-react';
import styled from 'react-emotion'
import RestoItem from './RestoItem';
import store from './store';

class RestoList extends React.Component<{}, {}> {
  componentDidMount() {
    store.getRestaurantList()
  }
  render() { 
    return (
      <List>
        {store.restoList.map((resto, index) => (
          <RestoItem key={index} resto={resto} />
        ))}
      </List>
    );
  }
}

const List = styled('div')`
  margin-bottom: 25px;
`

 
export default observer(RestoList);