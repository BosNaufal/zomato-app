import { observable, action } from 'mobx';
import { toast } from 'react-toastify';
import Req from 'src/api/base'

type PageParams = {
  start: number, 
  count: number,
}

class HomeStore {
  @observable restoList: [] = []
  @observable loading: boolean = false
  @observable params: PageParams = {
    start: 0,
    count: 10
  }

  @action setParams = (payload: Partial<PageParams>): void => {
    this.params = { ...this.params, ...payload }
  }

  setNextPageParams = () => {
    const { start, count } = this.params
    const nextStart = start + count
    this.setParams({ start: nextStart })    
  }

  @action setModelProps = (key: string, payload: Partial<HomeStore>): void => {
    this[key] = payload
  }

  @action setLoading = (payload: boolean) => {
    this.loading = payload
  }

  @action setRestoList = (payload: []) => {
    this.restoList = payload
  }

  @action getRestaurantList = async (): Promise<object | boolean> => {
    this.setLoading(true)
    const res = await Req('/search', {
      params: this.params,
    })
    if (res.error) {
      toast.error("Failed to proccess data...")
      return false
    } 
    const { restaurants } = res.data
    const flatList = restaurants.map((item: any) => item.restaurant)
    this.setRestoList(flatList)
    console.log(res.data, flatList)
    this.setLoading(false)
    // store.setModelProps("user", res.data)
    return res
  }
}

const store = new HomeStore()
export default store
