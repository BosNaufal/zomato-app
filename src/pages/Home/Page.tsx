import * as React from 'react';
import { hot } from 'react-hot-loader'

import RestoList from './RestoList'

const Page: React.SFC<{}> = () => {
  return ( 
    <div>
      <div className="mini-container">
        <RestoList />
      </div>
    </div>
  );
}

export default hot(module)(Page);