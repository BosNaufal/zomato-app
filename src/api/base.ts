
import axios from 'axios'

const baseUrl = `https://developers.zomato.com/api/v2.1`
const USER_KEY = 'bdc79d85698db50e74ee387b9aa52814'

export const Req = (url: string, options: object = {/**/}) => (
  axios({
    url: `${baseUrl}${url}`,
    headers: {
      "user-key": USER_KEY
    },
    ...options
  })
  .catch(({ response }: any) => ({ error: true, ...response }))
)

export default Req