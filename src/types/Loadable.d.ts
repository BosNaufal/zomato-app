declare module "react-loadable" {
  import * as React from "react";
  import { RouteComponentProps } from "react-router-dom";
  export interface ILoadableArguments {
    loader: () => any,
    loading: () => any
  }
  export default function(arg: ILoadableArguments): React.ComponentType<RouteComponentProps<any>> | React.ComponentType<any>
}


